
<div align="center">
    <img src="logo.png" alt="Coffee Logo" width="256" />
    <h1>Coding Challenges</h1>
    <p>A collection of awesome websites to do some coding challenges.</p>
    <p>Discover LeetCode, HackerRank, CodeWars, Exercism, CodeSignal Arcade, and more to come :)</p>
    <a href="https://leetcode.com/"><img src="docs/leetcode.png" alt="LeetCode Logo" width="128" /></a>
    <a href="https://www.hackerrank.com/"><img src="docs/hackerrank.png" alt="HackerRank Logo" width="128" /></a>
    <a href="https://www.codewars.com/"><img src="docs/codewars.png" alt="CodeWars Logo" width="128" /></a>
    <a href="https://exercism.org/"><img src="docs/exercism.png" alt="Exercism Logo" width="128" /></a>
    <a href="https://app.codesignal.com/arcade"><img src="docs/codesignal.png" alt="CodeSignal Logo" width="128" /></a>
</div>

# Credits

- [Thibault Charrin](https://gitlab.com/thibaultcharin) (_DevOps Consultant_)

  - Author, solutions

- [LeetCode](https://leetcode.com/), [HackerRank](https://www.hackerrank.com/), [CodeWars](https://www.codewars.com/), [Exercism](https://exercism.org/), [CodeSignal-Arcade](https://app.codesignal.com/arcade)

  - Community-driven code challenges, katas, solutions, logos

- [shaonianruntu](https://macosicons.com/#/u/shaonianruntu)

  - 'macOS-like' LeetCode Logo

- [nishant_rout](https://macosicons.com/#/u/nishant_rout)

  - 'macOS-like' HackerRank Logo

- [seeklogo](https://seeklogo.com/vector-logo/338820/coffee-time)

  - Coffee Logo
  