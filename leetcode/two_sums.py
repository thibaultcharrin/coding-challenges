"""1. Two Sums."""

import sys


def run():
    """Test algorithm."""
    try:
        nums = sys.argv[1]
        target = sys.argv[2]
    except:
        print("using_default_values")
        nums = [2, 7, 11, 15]
        target = 9
    print(f"{nums=}")
    print(f"{target=}")
    print(twoSum(nums, target))


def twoSum(nums: list[int], target: int) -> list[int]:
    """Return the two indexes that match target.

    In Big-O notation, this solution provides a O(n) or linear time complexity.
    This means that we are doing a sequential search
    rather than a nested for loop which would result in a 0(n^2) or quadratic time complexity.
    """
    result: list = []
    cached_indexes: dict = {}
    for index, value in enumerate(nums):
        other_index: int = target - value
        if other_index in cached_indexes:
            result = [cached_indexes[other_index], index]
            break
        cached_indexes[value] = index
    return result


if __name__ == "__main__":
    run()
