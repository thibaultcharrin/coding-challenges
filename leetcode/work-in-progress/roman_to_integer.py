"""13. Roman to Integer."""

import sys


def run():
    """Test algorithm."""
    try:
        s = sys.argv[1]
    except:
        print("using_default_values")
        s = "MCMXCIV"
        target = 9
    print(f"{s=}")
    print(romanToInt(s))


def romanToInt(s: str) -> int:
    """Parse Roman to Integer."""
    symbols: dict = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
    result: int = 0
    previous_char: str | None = None
    for this_char in reversed(s):

        print(f"{this_char}: {symbols[this_char]}")

        if not previous_char or previous_char == "I":
            if this_char == "I":
                previous_char = "I"
                print("+1")
                result += symbols[this_char]
            if this_char != "I":
                if previous_char and symbols[this_char] >= symbols[previous_char]:
                    print(f"+{symbols[this_char]}")
                    result += symbols[this_char]
                    previous_char = None
                else:
                    previous_char = this_char
                    print("+0")
        else:
            if (
                (previous_char in ("V", "X") and this_char == "I")
                or (previous_char in ("L", "C") and this_char == "X")
                or (previous_char in ("D", "M") and this_char == "C")
            ):
                temp = symbols[previous_char] - symbols[this_char]
                print(f"+{temp}")
                result += temp
                previous_char = None
            else:
                temp = symbols[previous_char] + symbols[this_char]
                print(f"+{temp}")
                result += temp
                previous_char = None

    if previous_char and previous_char != "I":
        print(f"+{symbols[previous_char]}")
        result += symbols[previous_char]

    return result


if __name__ == "__main__":
    run()
