"""859. Buddy Strings."""

import copy
import sys


def run():
    """Test algorithm."""
    try:
        s = sys.argv[1]
        goal = sys.argv[2]
    except:
        print("using_default_values")
        s = "aaabc"
        goal = "aabac"
    print(f"{s=}")
    print(f"{goal=}")
    print(buddyStrings(s, goal))


def buddyStrings(s, goal):
    """Return whether you can swap two letters in s so the result is equal to goal."""
    if fail_fast(s, goal):
        print("fail_fast")
        return False
    s, goal = decomplexify(s, goal)
    if check_buddy_strings(s, goal):
        print("is_buddy_strings")
        return True
    else:
        print("is_not_buddy_strings")
        return False


def check_buddy_strings(s, goal):
    """Check buddy strings."""
    for i in range(0, len(s) - 1):
        for j in range(i + 1, len(s)):
            tmp_1 = copy.copy(s)
            tmp_1[i] = s[j]
            tmp_1[j] = s[i]

            if tmp_1 == goal:
                return True


def decomplexify(s, goal):
    """Decomplexify space."""
    s, goal = list(s), list(goal)
    if len(s) > 50:
        indices_to_pop = [i for i in range(0, len(s)) if s[i] == goal[i]]
        indices_to_pop.sort(reverse=True)

        if indices_to_pop:
            for i in indices_to_pop:
                if len(s) > 2:
                    s.pop(i)
                    goal.pop(i)
        print(f"after: {s=} {goal=}")
    return s, goal


def fail_fast(s, goal):
    """Fail fast."""
    if len(s) != len(goal):
        print("different_lengths")
        return True
    a, b = list(s), list(goal)
    a.sort()
    b.sort()
    if a != b:
        print("different_characters")
        return True


if __name__ == "__main__":
    run()
